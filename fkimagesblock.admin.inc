<?php

/**
 * $file
 * Admin page callbacks for FK Images Block module
 */

/**
 * Form for control panel
 */
function fkimagesblock_settings_form($form, &$form_state) {
  $form['fkimagesblock'] = array(
      '#type' => 'fieldset',
      '#title' => t('Control panel for FK Images Block module'),
  );
  $form['fkimagesblock']['display'] = array(
      '#type' => 'radios',
      '#title' => t('Display type'),
      '#description' => t('Select method to display image in block'),
      '#options' => array(
          0 => t('Slideshow'),
          1 => t('Display all without any effect')
      ),
      '#default_value' => variable_get('fkimagesblock_display_type', 0)
  );
  // setting for slideshow
  $form['fkimagesblock']['slideshow-settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings for slideshow'),
      '#states' => array(
          'visible' => array(
              ':input[name="display"]' => array('value' => 0)
          )
      )
  );
  $form['fkimagesblock']['slideshow-settings']['speed'] = array(
      '#type' => 'textfield',
      '#title' => t('Speed'),
      '#description' => t('Animation speed in millisecond (1000 millisecond = 1 second)'),
      '#element_validate' => array('element_validate_integer_positive'),
      '#default_value' => variable_get('fkimagesblock_animation_setting_speed', 4000)
  );
  $form['fkimagesblock']['slideshow-settings']['effect'] = array(
      '#type' => 'select',
      '#title' => t('Effect'),
      '#description' => t('Select effect of animation'),
      '#options' => array(
          0 => t('show/hide'),
          1 => t('fadeIn/hide'),
          2 => t('slideUp/slideDown')
      ),
      '#default_value' => variable_get('fkimagesblock_animation_setting_effect', 0)
  );
  $form['fkimagesblock']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save')
  );

  return $form;
}

function fkimagesblock_settings_form_submit($form, &$form_state) {
  variable_set('fkimagesblock_display_type', $form_state['values']['display']);

  // save settings for animation
  if ($form_state['values']['display'] == 0) {
    variable_set('fkimagesblock_animation_setting_speed', $form_state['values']['speed']);
    variable_set('fkimagesblock_animation_setting_effect', $form_state['values']['effect']);
  }

  drupal_set_message(t('Settings was changed'));
}