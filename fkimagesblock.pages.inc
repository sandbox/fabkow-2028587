<?php

/**
 * $file
 * Manage page callbacks for FK Images Block module
 */

/**
 * Form for images (sortable)
 */
function fkimagesblock_list_form($form, &$form_state) {
  $form['image_items']['#tree'] = TRUE;

  // Get all the images from the database.
  $images = fkimagesblock_select_image($id = NULL, $language = NULL)->fetchAll();

  // Loop through the images and add them to the table.
  foreach ($images as $image) {
    $form['image_items'][$image->fkid] = array(
        'title' => array(
            '#markup' => check_plain($image->title)
        ),
        'weight' => array(
            '#type' => 'weight',
            '#delta' => 100,
            '#default_value' => $image->weight,
            '#attributes' => array('class' => array('fkimagesblock-weight'))
        ),
        'language' => array(
            '#markup' => check_plain($image->language)
        ),
        'status' => array(
            '#markup' => check_plain($image->status)
        ),
    );
  };

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  return $form;

  $header = array(
      t('Title'),
      t('Language'),
      array('data' => t('Operations'), 'colspan' => 3),
      t('Weight')
  );
  $rows = array();

  // Get all the images from the database.
  $images = fkimagesblock_select_image()->fetchAll();

  // Loop through the images and add them to the table.
  foreach ($images as $image) {
    $rows[] = array(
        check_plain($image->title),
        check_plain($image->language),
        l(t('Edit'), 'admin/structure/fkimagesblock/edit/' . $image->fkid),
        l(t('Delete'), 'admin/structure/fkimagesblock/delete/' . $image->fkid),
        ($image->status == 1) ? l(t('Disable'), 'admin/structure/fkimagesblock/disable/' . $image->fkid) : l(t('Enable'), 'admin/structure/fkimagesblock/enable/' . $image->fkid),
        $image->weight,
    );
  }

  if (!$rows) {
    $rows[] = array(array(
            'data' => t('No images available.'),
            'colspan' => 5,
    ));
  }

  $output['images_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
  );

  return $output;
}

/**
 * Theme to image items weight
 */
function theme_fkimagesblock_list_form($variables) {

  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form['image_items']) as $id) {
    $rows[] = array(
        'data' => array(
            drupal_render($form['image_items'][$id]['title']),
            drupal_render($form['image_items'][$id]['language']),
            l(t('Edit'), 'admin/structure/fkimagesblock/edit/' . $id),
            l(t('Delete'), 'admin/structure/fkimagesblock/delete/' . $id),
            (drupal_render($form['image_items'][$id]['status']) == 1) ? l(t('Disable'), 'admin/structure/fkimagesblock/disable/' . $id) : l(t('Enable'), 'admin/structure/fkimagesblock/enable/' . $id),
            drupal_render($form['image_items'][$id]['weight'])
        ),
        'class' => array('draggable'),
    );
  }
  if (!$rows) {
    $rows[] = array(array(
            'data' => t('No images available.'),
            'colspan' => 6,
    ));
  }

  $header = array(
      t('Title'),
      t('Language'),
      array('data' => t('Operations'), 'colspan' => 3),
      t('Weight')
  );

  $table_id = 'fkimagesblock-table';

  $output = '<h2>' . t("List of images") . '</h2>';
  $output .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('id' => $table_id))
  );
  $output .= drupal_render_children($form);
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'fkimagesblock-weight');

  return $output;
}

function fkimagesblock_list_form_submit($form, &$form_state) {
  foreach ($form_state['values']['image_items'] as $id => $image) {
    db_update('fkimagesblock')
            ->fields(array(
                'weight' => $image['weight']
                    )
            )
            ->condition('fkid', $id)
            ->execute();
    drupal_set_message(t('Weight was changed'));
    $form_state['redirect'] = 'admin/structure/fkimagesblock';
  }
}

/**
 * Form for operation on images (add, edit, delete, disable or enable status)
 * 
 * @param $operation - type of operation on image (add, edit, delete, disable or enable status)
 * @param $what - ID of image (edit, delete, disable or enable status) or parameter 'new' in form add
 *
 *   
 */
function fkimagesblock_list_operation_form($form, &$form_state, $operation, $what) {
  switch ($operation) {
    case 'add':
    case 'edit':

      $form['fieldset'] = array(
          '#type' => 'fieldset',
          '#title' => t('Enter information about image'),
      );

      if ($operation == 'edit') {
        $image_info = fkimagesblock_select_image($what, $language = NULL)->fetchAssoc();
        if (!empty($image_info)) {
          $img = array(
              'title' => $image_info['title'],
              'subtitle' => $image_info['subtitle'],
              'url' => $image_info['url'],
              'target' => $image_info['target'],
              'alt' => $image_info['alt'],
              'file_id' => $image_info['file_id'],
              'language' => $image_info['language'],
              'weight' => $image_info['weight'],
              'status' => $image_info['status'],
          );
        }
      }

      $form['fieldset']['operation'] = array(
          '#type' => 'hidden',
          '#value' => $operation,
      );

      $form['fieldset']['what'] = array(
          '#type' => 'hidden',
          '#value' => $what,
      );

      $form['fieldset']['title'] = array(
          '#type' => 'textfield',
          '#title' => t('Title'),
          '#maxlength' => 255,
          '#default_value' => (isset($img['title'])) ? $img['title'] : '',
          '#required' => TRUE,
      );

      $form['fieldset']['subtitle'] = array(
          '#type' => 'textfield',
          '#title' => t('Subtitle'),
          '#maxlength' => 255,
          '#default_value' => (isset($img['subtitle'])) ? $img['subtitle'] : '',
      );

      $form['fieldset']['url'] = array(
          '#type' => 'textfield',
          '#title' => t('URL address'), '#description' => t('The link for this banner (leave empty if You want display image without link). This can be an internal Drupal path such as %add-node or an external URL such as %drupal.', array('%add-node' => 'node/add', '%drupal' => 'http://drupal.org')),
          '#default_value' => (isset($img['url'])) ? $img['url'] : '',
      );

      $form['fieldset']['target'] = array(
          '#type' => 'select',
          '#title' => t('Choose target of url'),
          '#options' => array(
              0 => t('_self (default value)'),
              1 => t('_blank'),
              2 => t('_parent'),
              3 => t('_top'),
          ),
          '#default_value' => (isset($img['target'])) ? $img['target'] : 0,
      );

      $form['fieldset']['alt'] = array(
          '#type' => 'textfield',
          '#title' => t('Alt attribute'),
          '#maxlength' => 255,
          '#default_value' => (isset($img['alt'])) ? $img['alt'] : '',
      );

      $form['fieldset']['file_id'] = array(
          '#title' => t('Image'),
          '#type' => 'managed_file',
          '#upload_location' => 'public://fkimagesblock/',
          '#description' => t('Only files with the following extensions are allowed: gif, png, jpg, jpeg.'),
          '#upload_validators' => array(
              'file_validate_extensions' => array('gif png jpg jpeg'),
          ),
          '#default_value' => (!empty($img['file_id'])) ? $img['file_id'] : NULL,
          '#required' => TRUE,
      );
      $form['#attributes']['enctype'] = 'multipart/form-data';

      // Language filter if there is a list of languages
      if ($languages = module_invoke('locale', 'language_list')) {
        $languages = array(LANGUAGE_NONE => t('Language neutral')) + $languages;
        $form['fieldset']['language'] = array(
            '#type' => 'select',
            '#title' => t('Language'),
            '#options' => array(
        'und' => t('Neutral'),
            ) + $languages,
            '#default_value' => (!empty($img['language'])) ? $img['language'] : 'und',
        );
      }
      $form['fieldset']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Submit'),
      );
      break;

    case 'delete':
      $form['fieldset']['operation'] = array(
          '#type' => 'value',
          '#value' => $operation,
      );
      $form['fieldset']['what'] = array(
          '#type' => 'value',
          '#value' => $what,
      );

      return confirm_form(
              $form, t('Are you sure you want to delete image?'), 'admin/structure/fkimagesblock', t('This action cannot be undone.'), t('Delete'), t('Cancel')
      );
      break;

    case 'disable':
      $status = db_update('fkimagesblock')
              ->fields(array(
                  'status' => 0
                      )
              )
              ->condition('fkid', $what)
              ->execute();
      drupal_set_message(t('Image was disable'));
      drupal_goto('admin/structure/fkimagesblock');
      break;

    case 'enable':
      $status = db_update('fkimagesblock')
              ->fields(array(
                  'status' => 1
                      )
              )
              ->condition('fkid', $what)
              ->execute();
      drupal_set_message(t('Image was enable'));
      drupal_goto('admin/structure/fkimagesblock');
      break;
  }

  return $form;
}

function fkimagesblock_list_operation_form_submit($form, &$form_state) {
  $operation = $form_state['values']['operation'];
  $what = $form_state['values']['what'];

  switch ($operation) {
    case 'add':
    case 'edit':
      if (!form_get_errors()) {
        if (!empty($form_state['values']['file_id'])) {
          $file_id = file_load($form_state['values']['file_id']);
          $file_id->status = FILE_STATUS_PERMANENT;
          file_save($file_id);
          file_usage_add($file_id, 'fkimagesblock', 'image', 1);
          $image = image_load($file_id->uri);
          image_save($image);
        }
        /* to database */
        $entry = array(
            'title' => (isset($form_state['values']['title'])) ? $form_state['values']['title'] : '',
            'subtitle' => (isset($form_state['values']['subtitle'])) ? $form_state['values']['subtitle'] : '',
            'url' => (isset($form_state['values']['url'])) ? $form_state['values']['url'] : '',
            'target' => $form_state['values']['target'],
            'alt' => (isset($form_state['values']['alt'])) ? $form_state['values']['alt'] : '',
            'language' => $form_state['values']['language'],
            'file_id' => (isset($form_state['values']['file_id'])) ? $form_state['values']['file_id'] : 0,
        );

        if ($operation == 'add') {
          $insert_values = db_insert('fkimagesblock')
                  ->fields($entry)
                  ->execute();
          drupal_set_message(t('Image was added'));
        } else if ($operation = 'edit') {
          $update_values = db_update('fkimagesblock')
                  ->fields($entry)
                  ->condition('fkid', $what)
                  ->execute();
          drupal_set_message(t('Image was edited'));
        }
        $form_state['redirect'] = 'admin/structure/fkimagesblock';
      }

      break;

    case 'delete':
      // get file id
      $image_info = fkimagesblock_select_image($what, $language = NULL)->fetchAssoc();
      if (!empty($image_info)) {
        $file_id = $image_info['file_id'];
      }

      // delete file from database
      file_delete(file_load($file_id), $force = TRUE);

      db_delete('fkimagesblock')
              ->condition('fkid', $what)
              ->execute();

      drupal_set_message(t('Image has been deleted.'));
      watchdog('fkimagesblock', 'Image has been deleted.');

      $form_state['redirect'] = 'admin/structure/fkimagesblock';

      break;
  }
}