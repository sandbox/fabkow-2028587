<?php
/**
 * @file fkimagesblock.tpl.php
 *
 * Theme implementation to display the FK Images Block
 *
 * Available variables:
 * - $images - (array)
 *
 * @see template_preprocess_fkimagesblock()
 */
?>

<div id="fkimagesblock">
  <?php if ($images): ?>
    <?php foreach ($images as $image): ?>
      <div class="fkimageblock fkimageblock<?php print $image['id']; ?>">
        <h5><?php print $image['title']; ?></h5>
        <p><?php print $image['subtitle']; ?></p>
        <?php print (!empty($image['href'])) ? l('<img src="' . $image['file_url'] . '" alt="' . $image['alt'] . '" />', $image['href'], array('attributes' => array('target' => $image['target']), 'html' => TRUE)) : '<img src="' . $image['file_url'] . '" alt="' . $image['alt'] . '" />'; ?>
      </div>
    <?php endforeach; ?>
  <?php else: ?>
    <?php print t('No image available'); ?>
  <?php endif; ?>
</div>
