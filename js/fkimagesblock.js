(function($){
    $(window).load(function() {
      var effect_start = Drupal.settings.fkimagesblock.effect_start;
      var effect_end = Drupal.settings.fkimagesblock.effect_end;
      
      if($('#fkimagesblock .fkimageblock').length>1){
        $('#fkimagesblock .fkimageblock').first()[effect_start]();
        setInterval(function(){
          $('#fkimagesblock .fkimageblock').first()[effect_end]().appendTo('#fkimagesblock');
          $('#fkimagesblock .fkimageblock').first()[effect_start]();
        }, Drupal.settings.fkimagesblock.speed);
      }else{
        $('#fkimagesblock .fkimageblock').first()[effect_start]();
      }
    });
})(jQuery);
